cwd = File.dirname(__FILE__)
with_chef_local_server :chef_repo_path => '../chef_repo',
                       :cookbook_path  => [ File.join(cwd, 'cookbooks')]
