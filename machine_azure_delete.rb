require 'chef/provisioning/azure_driver'
with_driver 'azure'

machine 'chefProvisionDemo' do
  action :destroy
end

machine 'chefprovAnother' do
  action :destroy
end
