log_level                :warn
cwd = File.dirname(__FILE__)
node_name                "provisioner"
client_key               "#{cwd}/dummy.pem"
validation_client_name   "validator"

drivers({
  "vagrant:#{cwd}/vms" =>{
    :machine_options => {
      :vagrant_options => {
        'vm.box' => 'chef/ubuntu-14.10'
      }
    }
  },

  "ssh" =>{
    :machine_options => {
      :transport_options => {
        :is_windows => false,
        :ip_address => '192.168.88.135',
        :host => 'vagrant-demo',
        :username => 'vagrant',
        :ssh_options => {
          :password => 'vagrant'
        }
      }
    }
  }

  })
