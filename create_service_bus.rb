require 'chef/mixin/shell_out'
require 'json'

sb_namespace = "bbs-game-azure-globalcamp2015"
azure_region = "\"Southeast Asia\""

checkAvail_ret_json = JSON.parse(`azure sb namespace check #{sb_namespace} --json`)

if checkAvail_ret_json["available"]
  begin
    create_ret = `azure sb namespace create --json #{sb_namespace} #{azure_region}`
    create_json = JSON.parse(create_ret)
    if create_json["createACSNamespace"]
      Chef::Log.info("success create #{sb_namespace} in #{azure_region}")
    else
      Chef::Log.fatal("create service bus namespace rror: ret=#{create_json}")
    end
  rescue => ex
    Chef::Log.fatal("craate service bus namespace rror: ex=\n", ex.message, "\n")
  end
else
  Chef::Log.info("namespace #{sb_namespace} already in use!")
end
