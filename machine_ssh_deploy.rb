require 'chef/provisioning'
require 'chef/provisioning/ssh_driver'

with_driver 'ssh'
machine 'vagrant-demo' do
    action [:ready, :setup, :converge]
    run_list ['helloworld']
    converge true
end
