require 'chef/provisioning/vagrant_driver'
# vagrant_box 'chef/ubuntu-14.10' do
#    # url 'https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box'
#    url 'https://atlas.hashicorp.com/chef/boxes/ubuntu-14.10/versions/1.0.0/providers/virtualbox.box'
# end

with_driver 'vagrant'
machine 'vagrant-demo' do
  run_list ['helloworld']
end
