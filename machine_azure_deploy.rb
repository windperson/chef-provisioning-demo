require 'chef/provisioning/azure_driver'
with_driver 'azure'

acs_name = 'chef-provisioning-demo-azure-bootcamp2015-tpe'

machine 'chefProvisionDemo' do
  machine_options ({
    :bootstrap_options => {
      :cloud_service_name => "#{acs_name}", #required
      :storage_account_name => 'chefprov', #required
      :vm_size => "Medium", #required
      :location => 'Southeast Asia', #required
      # :tcp_endpoints => '80:80,3000:3000', #optional
    },
    :image_id => 'b39f27a8b8c64d52b05eac6a62ebad85__Ubuntu-14_04_2_LTS-amd64-server-20150309-en-us-30GB', #required
    :password => "A2ureDem0" #required
    })
    # attributes ({ "vm_hostname" => 'MyVM1' })
    run_list ['AzureLinuxAgent','helloworld']
  end

vm2nd = 'chefprovAnother'

machine "#{vm2nd}" do
  vm_name = 'MyVM2'
  machine_options ({
    :bootstrap_options => {
      :cloud_service_name => "#{acs_name}", #required
      :storage_account_name => 'chefprov', #required
      :vm_size => "Medium", #required
      :location => 'Southeast Asia', # required
      # :tcp_endpoints => ':81', #optional
    },
    :image_id => 'b39f27a8b8c64d52b05eac6a62ebad85__Ubuntu-14_04_2_LTS-amd64-server-20150309-en-us-30GB', #required
    :password => "A2ureDem0" #required
    })
    # attributes ({ "vm_hostname" => "#{vm_name}" })
    run_list ['AzureLinuxAgent']
end
